# ComPDF

[![CI Status](https://img.shields.io/travis/GgnSwaitch/ComPDF.svg?style=flat)](https://travis-ci.org/GgnSwaitch/ComPDF)
[![Version](https://img.shields.io/cocoapods/v/ComPDF.svg?style=flat)](https://cocoapods.org/pods/ComPDF)
[![License](https://img.shields.io/cocoapods/l/ComPDF.svg?style=flat)](https://cocoapods.org/pods/ComPDF)
[![Platform](https://img.shields.io/cocoapods/p/ComPDF.svg?style=flat)](https://cocoapods.org/pods/ComPDF)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ComPDF is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ComPDF'
```

## Author

GgnSwaitch, gswaitch@comdain.com.au

## License

ComPDF is available under the MIT license. See the LICENSE file for more info.
